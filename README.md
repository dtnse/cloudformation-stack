# Task

Your task is to create and deploy a CloudFormation stack in the US East 2 (Ohio)
region using the temporary AWS account provided to you by DTN.

The CloudFormation stack should provision:

 * a VPC with one public and one private subnet
 * a NAT gateway in the public subnet to enable outbound internet traffic from the private subnet
 * a basic auto-scaling LAMP stack using t2.micro EC2 and RDS instances

The LAMP stack's HTTP URL should return a DTN-themed image of your choice.
